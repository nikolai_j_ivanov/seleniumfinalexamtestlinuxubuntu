import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static org.testng.Assert.*;

public class ExamSolution {
    private WebDriver driver;

    @BeforeMethod
    public void setup() {
        System.setProperty("webdriver.chrome.driver", "/home/nikolay/drivers/chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //login
        driver.get("http://shop.pragmatic.bg/");
    }


    @Test
    public void accountCreationPragmaticShopPositive() {
        //navigate to the Register Account page
        driver.findElement(By.cssSelector("div#top-links a.dropdown-toggle span.hidden-xs")).click();
        driver.findElement(By.linkText("Register")).click();
        //Make sure that you are in the right place if you want.
        assertEquals(driver.getTitle(), "Register Account");

        //entering the account data which will be submitted
        driver.findElement(By.id("input-firstname")).sendKeys("Srebren");
        driver.findElement(By.id("input-lastname")).sendKeys("Pishmirov");
        driver.findElement(By.id("input-email")).sendKeys("valdes" + RandomStringUtils.randomAlphanumeric(4) + "@abv.bg");
        driver.findElement(By.id("input-telephone")).sendKeys("123456789");
        driver.findElement(By.id("input-password")).sendKeys("parola");
        driver.findElement(By.id("input-confirm")).sendKeys("parola");

        //Get the Radio Button as WebElement using it's value attribute
        WebElement subscribeYes = driver.findElement(By.cssSelector("label.radio-inline input[value='1']"));

        //Check if its already selected? otherwise select the Radio Button
        //by calling click() method
        if (!subscribeYes.isSelected()) {
            subscribeYes.click();
        }
        //Verify Radio Button is selected
        assertTrue(subscribeYes.isSelected());

        WebElement checkboxPrivacyPolicy = driver.findElement(By.xpath("//a[@class='agree']/following-sibling::input[@type='checkbox']"));

        //Verify the privacy policy is not checked by default
        assertFalse(checkboxPrivacyPolicy.isSelected(), "The privacy policy was checked/accepted by default.");

        //check the privacy policy
        checkboxPrivacyPolicy.click();

        //Verify Checkbox is Selected
        assertTrue(checkboxPrivacyPolicy.isSelected());

        // click the continue button
        driver.findElement(By.xpath("//a[@class='agree']/following-sibling::input[@type='submit']")).click();

        //Assert that your account has been created
        assertEquals(driver.getTitle(), "Your Account Has Been Created!", "Your account was NOT created!");
    }

    @Test
    public void accountCreationPragmaticShopNegativeWithoutAccountDataProvided() {
        //navigate to the Register Account page
        driver.findElement(By.cssSelector("a.dropdown-toggle span.hidden-xs")).click();
        driver.findElement(By.linkText("Register")).click();
        //optional - you can make sure that you are in the right place if you want.
        assertEquals(driver.getTitle(), "Register Account", "You are currently not in the register account page!");

        //We are skipping the entering of the account data because the point of this negative test is to
        // verify the error messages in case we don't enter account data when trying to create an account

        // So directly we are checking the privacy policy checkbox
        WebElement checkboxPrivacyPolicy = driver.findElement(By.xpath("//a[@class='agree']/following-sibling::input[@type='checkbox']"));
        //check the privacy policy
        checkboxPrivacyPolicy.click();
        //Verify Checkbox is Selected
        assertTrue(checkboxPrivacyPolicy.isSelected());

        // click the continue button
        driver.findElement(By.xpath("//a[@class='agree']/following-sibling::input[@type='submit']")).click();

        //VERIFY the ERROR messages DISPLAYED due to the missing account data when trying to create an account
        String actualFirstNameErrorMessage = driver.findElement(By.cssSelector("input#input-firstname+div")).getText();
        assertEquals(actualFirstNameErrorMessage, "First Name must be between 1 and 32 characters!");

        String actualLastNameErrorMessage = driver.findElement(By.cssSelector("input#input-lastname+div")).getText();
        assertEquals(actualLastNameErrorMessage, "Last Name must be between 1 and 32 characters!");

        String actualEmailErrorMessage = driver.findElement(By.cssSelector("input#input-email+div")).getText();
        assertEquals(actualEmailErrorMessage, "E-Mail Address does not appear to be valid!");

        String actualPhoneErrorMessage = driver.findElement(By.cssSelector("input#input-telephone+div")).getText();
        assertEquals(actualPhoneErrorMessage, "Telephone must be between 3 and 32 characters!");

        String actualPasswordErrorMessage = driver.findElement(By.cssSelector("input#input-password+div")).getText();
        assertEquals(actualPasswordErrorMessage, "Password must be between 4 and 20 characters!");
    }

    @Test
    public void loginUsingExistingAccountPositive() {
        //navigate to the Login page
        driver.findElement(By.cssSelector("a.dropdown-toggle span.hidden-xs")).click();
        driver.findElement(By.linkText("Login")).click();
        //make sure that you are in the right place.
        assertEquals(driver.getTitle(), "Account Login", "You are currently not in the Login Account page!");

        //Entering credentials and click login
        driver.findElement(By.id("input-email")).sendKeys("valdes7k0U@abv.bg");
        driver.findElement(By.id("input-password")).sendKeys("parola");
        driver.findElement(By.xpath("//input[@id='input-password']//..//following-sibling::input[@value='Login']")).click();

        //Asserting that we were successfully logged in
        assertEquals(driver.getTitle(), "My Account", "The login was unsuccessful!");
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }
}